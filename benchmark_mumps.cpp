#include <iostream>
#include <fstream>
#include <chrono>
#include <vector>
#include <dmumps_c.h>
#include <mpi.h>

void solveLinearSystem(int* irn, int* jcn, double* a, double* rhs, double* solution, int nnz, int size) {
    MUMPS_INT n = size;
    MUMPS_INT nz = nnz;
    MUMPS_INT* irn_loc = new MUMPS_INT[nnz];
    MUMPS_INT* jcn_loc = new MUMPS_INT[nnz];
    double* a_loc = new double[nnz];
    MUMPS_INT* perm = new MUMPS_INT[size];

    // MUMPS configuration
    DMUMPS_STRUC_C id;
    id.job = -1;
    id.par = 1;
    id.sym = 0;
    id.comm_fortran = MPI_COMM_WORLD;
    dmumps_c(&id);

    // Copy sparse matrix data
    std::copy(irn, irn + nnz, irn_loc);
    std::copy(jcn, jcn + nnz, jcn_loc);
    std::copy(a, a + nnz, a_loc);

    // MUMPS analysis + factorization + solve
    id.job = 6;
    id.icntl[7-1] = 6; // 2:amf 4:pord 5:metis 6:qamd

    id.n = n;
    id.nz = nz;
    id.irn = irn_loc;
    id.jcn = jcn_loc;
    id.a = a_loc;

    id.rhs = rhs;
    id.nrhs = 1;

    id.sol_loc = solution;
    id.lsol_loc = size;
    dmumps_c(&id);

    // Clean up
    delete[] irn_loc;
    delete[] jcn_loc;
    delete[] a_loc;
    delete[] perm;
}


int main() {
  int nnz = 0;
  int size = 0;
  std::vector<int> irn, jcn;
  std::vector<double> a;
//  {
//    std::ifstream file("K_coo.txt");
//    int col1, col2;
//    double col3;
//    while (file >> col1 >> col2 >> col3) {
//      irn.push_back(col1+1);
//      jcn.push_back(col2+1);
//      a.push_back(col3);
//      ++nnz;
//      if (col1 > size)
//        size = col1;
//    }
//    ++size;
//  }

//  std::ofstream fout{"K_coo.bin", std::ios::out | std::ios::binary};
//  fout.write((char*)&size, sizeof(size));
//  fout.write((char*)&nnz, sizeof(nnz));
//  for (const auto& value : irn)
//    fout.write(reinterpret_cast<const char*>(&value), sizeof(value));
//  for (const auto& value : jcn)
//    fout.write(reinterpret_cast<const char*>(&value), sizeof(value));
//  for (const auto& value : a)
//    fout.write(reinterpret_cast<const char*>(&value), sizeof(value));
//  fout.close();

  std::ifstream fin{"K_coo.bin", std::ios::in | std::ios::binary};
  fin.read(reinterpret_cast<char*>(&size), sizeof(size));
  fin.read(reinterpret_cast<char*>(&nnz), sizeof(nnz));
  irn.resize(nnz);
  jcn.resize(nnz);
  a.resize(nnz);
  for (auto& value : irn)
    fin.read(reinterpret_cast<char*>(&value), sizeof(value));
  for (auto& value : jcn)
    fin.read(reinterpret_cast<char*>(&value), sizeof(value));
  for (auto& value : a)
    fin.read(reinterpret_cast<char*>(&value), sizeof(value));

  double* rhs = new double[size];
  double* solution = new double[size];
  for (int i = 0; i < size; ++i) {
    rhs[i] = rand() / (RAND_MAX + 1.0);
  }

  std::cout << "Problem Size: " << size << std::endl << "nnz = " << nnz << std::endl;

  auto startTime = std::chrono::high_resolution_clock::now();
  solveLinearSystem(&(irn[0]), &(jcn[0]), &(a[0]), rhs, solution, nnz, size);
  auto endTime = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> duration = endTime - startTime;
  std::cout.flush();
  std::cout << std::endl << std::endl << "BENCHMARK MUMPS TIME: " << duration.count() << " seconds" << std::endl << std::endl << std::endl << std::endl;

  // Clean up
  delete[] rhs;
  delete[] solution;

  return 0;
}