rm -rf OpenBLAS-0.3.26.tar.gz
wget https://github.com/OpenMathLib/OpenBLAS/releases/download/v0.3.26/OpenBLAS-0.3.26.tar.gz
rm -rf OpenBLAS-0.3.26
tar -zxf OpenBLAS-0.3.26.tar.gz
cd OpenBLAS-0.3.26
CC=gcc USE_THREAD=0 USE_OPENMP=0 INTERFACE64=0 MAX_THREADS=8 make -j4
CC=gcc USE_THREAD=0 USE_OPENMP=0 INTERFACE64=0 MAX_THREADS=8 make PREFIX=$(dirname $(pwd))/openblas-serial install
cd ..
rm -rf OpenBLAS-0.3.26
tar -zxf OpenBLAS-0.3.26.tar.gz
cd OpenBLAS-0.3.26
CC=gcc USE_THREAD=1 USE_OPENMP=0 INTERFACE64=0 MAX_THREADS=8 make -j4
CC=gcc USE_THREAD=1 USE_OPENMP=0 INTERFACE64=0 MAX_THREADS=8 make PREFIX=$(dirname $(pwd))/openblas-pthread install
cd ..
rm -rf OpenBLAS-0.3.26
tar -zxf OpenBLAS-0.3.26.tar.gz
cd OpenBLAS-0.3.26
CC=gcc USE_THREAD=1 USE_OPENMP=1 INTERFACE64=0 MAX_THREADS=8 make -j4
CC=gcc USE_THREAD=1 USE_OPENMP=1 INTERFACE64=0 MAX_THREADS=8 make PREFIX=$(dirname $(pwd))/openblas-openmp install
cd ..