wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-5.1.0.tar.gz

tar -zxf metis-5.1.0.tar.gz

export INSTALLDIR=$(pwd)/metis
rm -rf $INSTALLDIR

cd metis-5.1.0
make config prefix=$INSTALLDIR cc=gcc
make -j4
make install
cd ..