wget http://deb.debian.org/debian/pool/main/m/mumps/mumps_5.6.2.orig.tar.gz

export METISDIR=$(pwd)/metis
export STATICBLAS=$(pwd)/openblas-serial/lib/libopenblas.a
export INSTALLDIR=$(pwd)/mumps_serial_openblas_serial
rm -rf $INSTALLDIR
rm -rf MUMPS_5.6.2
tar -zxf mumps_5.6.2.orig.tar.gz
cd MUMPS_5.6.2
cp Make.inc/Makefile.inc.generic.SEQ Makefile.inc
sed -i '/^#\(LMETISDIR.*\)=.*/s//\1 = $${METISDIR}\/lib/' Makefile.inc
sed -i '/^#\(IMETIS.*\)=.*/s//\1 = -I$${METISDIR}\/include/' Makefile.inc
sed -i '/^#\(LMETIS .*(LMETISDIR) -lmetis.*\)/s//\1/' Makefile.inc
sed -i '/^\(ORDERINGSF .*\)\(-Dpord$\)/s//\1-Dmetis \2/' Makefile.inc
sed -i '/^\(CC.*= \)\(cc\)/s//\1gcc/' Makefile.inc
sed -i '/^\(FC.*= \)\(f90\)/s//\1gfortran/' Makefile.inc
sed -i '/^\(FL.*= \)\(f90\)/s//\1gfortran/' Makefile.inc
sed -i '/^\(RANLIB .*ranlib.*\)/s//#\1/' Makefile.inc
sed -i '/^#\(RANLIB .*echo.*\)/s//\1/' Makefile.inc
sed -i '/^\(LAPACK.*=\).*/s//\1/' Makefile.inc
sed -i '/^\(LIBBLAS.*=\).*/s//\1 $${STATICBLAS}/' Makefile.inc
sed -i '/^\(LIBOTHERS.*pthread\).*/s//#\1/' Makefile.inc
sed -i '/^\(OPTF.*=.*\)-O\(\>.*\)/s//\1-fallow-argument-mismatch -O3 -fPIC\2/' Makefile.inc
sed -i '/^\(OPT[CL].*=.*\)-O\(\>.*\)/s//\1-O3 -fPIC -DWITHOUT_PTHREAD=1\2/' Makefile.inc
make -j4 d
mkdir -p $INSTALLDIR
cp -r lib $INSTALLDIR/
cp libseq/*.a $INSTALLDIR/lib/
cp -r include $INSTALLDIR/
cp libseq/mpi.h $INSTALLDIR/include/
cd ..

export STATICBLAS=$(pwd)/openblas-pthread/lib/libopenblas.a
export INSTALLDIR=$(pwd)/mumps_serial_openblas_pthread
rm -rf $INSTALLDIR
rm -rf MUMPS_5.6.2
tar -zxf mumps_5.6.2.orig.tar.gz
cd MUMPS_5.6.2
cp Make.inc/Makefile.inc.generic.SEQ Makefile.inc
sed -i '/^#\(LMETISDIR.*\)=.*/s//\1 = $${METISDIR}\/lib/' Makefile.inc
sed -i '/^#\(IMETIS.*\)=.*/s//\1 = -I$${METISDIR}\/include/' Makefile.inc
sed -i '/^#\(LMETIS .*(LMETISDIR) -lmetis.*\)/s//\1/' Makefile.inc
sed -i '/^\(ORDERINGSF .*\)\(-Dpord$\)/s//\1-Dmetis \2/' Makefile.inc
sed -i '/^\(CC.*= \)\(cc\)/s//\1gcc/' Makefile.inc
sed -i '/^\(FC.*= \)\(f90\)/s//\1gfortran/' Makefile.inc
sed -i '/^\(FL.*= \)\(f90\)/s//\1gfortran/' Makefile.inc
sed -i '/^\(RANLIB .*ranlib.*\)/s//#\1/' Makefile.inc
sed -i '/^#\(RANLIB .*echo.*\)/s//\1/' Makefile.inc
sed -i '/^\(LAPACK.*=\).*/s//\1/' Makefile.inc
sed -i '/^\(LIBBLAS.*=\).*/s//\1 $${STATICBLAS}/' Makefile.inc
sed -i '/^\(OPTF.*=.*\)-O\(\>.*\)/s//\1-fallow-argument-mismatch -O3 -fPIC\2/' Makefile.inc
sed -i '/^\(OPT[CL].*=.*\)-O\(\>.*\)/s//\1-O3 -fPIC\2/' Makefile.inc
make -j4 d
mkdir -p $INSTALLDIR
cp -r lib $INSTALLDIR/
cp libseq/*.a $INSTALLDIR/lib/
cp -r include $INSTALLDIR/
cp libseq/mpi.h $INSTALLDIR/include/
cd ..


export STATICBLAS=$(pwd)/openblas-openmp/lib/libopenblas.a
export INSTALLDIR=$(pwd)/mumps_serial_openblas_openmp
rm -rf $INSTALLDIR
rm -rf MUMPS_5.6.2
tar -zxf mumps_5.6.2.orig.tar.gz
cd MUMPS_5.6.2
cp Make.inc/Makefile.inc.generic.SEQ Makefile.inc
sed -i '/^#\(LMETISDIR.*\)=.*/s//\1 = $${METISDIR}\/lib/' Makefile.inc
sed -i '/^#\(IMETIS.*\)=.*/s//\1 = -I$${METISDIR}\/include/' Makefile.inc
sed -i '/^#\(LMETIS .*(LMETISDIR) -lmetis.*\)/s//\1/' Makefile.inc
sed -i '/^\(ORDERINGSF .*\)\(-Dpord$\)/s//\1-Dmetis \2/' Makefile.inc
sed -i '/^\(CC.*= \)\(cc\)/s//\1gcc/' Makefile.inc
sed -i '/^\(FC.*= \)\(f90\)/s//\1gfortran/' Makefile.inc
sed -i '/^\(FL.*= \)\(f90\)/s//\1gfortran/' Makefile.inc
sed -i '/^\(RANLIB .*ranlib.*\)/s//#\1/' Makefile.inc
sed -i '/^#\(RANLIB .*echo.*\)/s//\1/' Makefile.inc
sed -i '/^\(LAPACK.*=\).*/s//\1/' Makefile.inc
sed -i '/^\(LIBBLAS.*=\).*/s//\1 $${STATICBLAS}/' Makefile.inc
sed -i '/^\(OPTF.*=.*\)-O\(\>.*\)/s//\1-fallow-argument-mismatch -O3 -fPIC\2/' Makefile.inc
sed -i '/^\(OPT[CL].*=.*\)-O\(\>.*\)/s//\1-O3 -fPIC\2/' Makefile.inc
make -j4 d
mkdir -p $INSTALLDIR
cp -r lib $INSTALLDIR/
cp libseq/*.a $INSTALLDIR/lib/
cp -r include $INSTALLDIR/
cp libseq/mpi.h $INSTALLDIR/include/
cd ..


# Note changes in OPTF, OPTC, OPTL
export STATICBLAS=$(pwd)/openblas-serial/lib/libopenblas.a
export INSTALLDIR=$(pwd)/mumps_openmp_openblas_serial
rm -rf $INSTALLDIR
rm -rf MUMPS_5.6.2
tar -zxf mumps_5.6.2.orig.tar.gz
cd MUMPS_5.6.2
cp Make.inc/Makefile.inc.generic.SEQ Makefile.inc
sed -i '/^#\(LMETISDIR.*\)=.*/s//\1 = $${METISDIR}\/lib/' Makefile.inc
sed -i '/^#\(IMETIS.*\)=.*/s//\1 = -I$${METISDIR}\/include/' Makefile.inc
sed -i '/^#\(LMETIS .*(LMETISDIR) -lmetis.*\)/s//\1/' Makefile.inc
sed -i '/^\(ORDERINGSF .*\)\(-Dpord$\)/s//\1-Dmetis \2/' Makefile.inc
sed -i '/^\(CC.*= \)\(cc\)/s//\1gcc/' Makefile.inc
sed -i '/^\(FC.*= \)\(f90\)/s//\1gfortran/' Makefile.inc
sed -i '/^\(FL.*= \)\(f90\)/s//\1gfortran/' Makefile.inc
sed -i '/^\(RANLIB .*ranlib.*\)/s//#\1/' Makefile.inc
sed -i '/^#\(RANLIB .*echo.*\)/s//\1/' Makefile.inc
sed -i '/^\(LAPACK.*=\).*/s//\1/' Makefile.inc
sed -i '/^\(LIBBLAS.*=\).*/s//\1 $${STATICBLAS}/' Makefile.inc
sed -i '/^\(OPTF.*=.*\)-O\(\>.*\)/s//\1-fallow-argument-mismatch -O3 -fopenmp -fPIC\2/' Makefile.inc
sed -i '/^\(OPT[CL].*=.*\)-O\(\>.*\)/s//\1-O3 -fopenmp -fPIC\2/' Makefile.inc
make -j4 d
mkdir -p $INSTALLDIR
cp -r lib $INSTALLDIR/
cp libseq/*.a $INSTALLDIR/lib/
cp -r include $INSTALLDIR/
cp libseq/mpi.h $INSTALLDIR/include/
cd ..
