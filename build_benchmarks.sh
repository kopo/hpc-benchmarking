echo "downloading benchmark matrix"
#wget https://www.student.dtu.dk/~kopo/tmp/K_coo.txt
wget https://www.student.dtu.dk/~kopo/tmp/K_coo.bin

echo "compiling benchmarks"
export MUMPSDIR=$(pwd)/mumps_serial_openblas_serial
export OPENBLASDIR=$(pwd)/openblas-serial
g++ -I $MUMPSDIR/include -O2 benchmark.cpp\
   $MUMPSDIR/lib/libdmumps.a\
   $MUMPSDIR/lib/libmumps_common.a\
   $MUMPSDIR/lib/libpord.a\
   $MUMPSDIR/lib/libmpiseq.a\
   $OPENBLASDIR/lib/libopenblas.a\
   -L$(pwd)/metis/lib -lmetis -lgfortran\
   -o benchmark_mumps_serial_openblas_serial


export MUMPSDIR=$(pwd)/mumps_serial_openblas_pthread
export OPENBLASDIR=$(pwd)/openblas-pthread
g++ -I $MUMPSDIR/include -O2 benchmark.cpp\
   $MUMPSDIR/lib/libdmumps.a\
   $MUMPSDIR/lib/libmumps_common.a\
   $MUMPSDIR/lib/libpord.a\
   $MUMPSDIR/lib/libmpiseq.a\
   $OPENBLASDIR/lib/libopenblas.a\
   -L$(pwd)/metis/lib -lmetis -lpthread -lgfortran\
   -o benchmark_mumps_serial_openblas_pthread


export MUMPSDIR=$(pwd)/mumps_serial_openblas_openmp
export OPENBLASDIR=$(pwd)/openblas-openmp
g++ -I $MUMPSDIR/include -O2 benchmark.cpp\
   $MUMPSDIR/lib/libdmumps.a\
   $MUMPSDIR/lib/libmumps_common.a\
   $MUMPSDIR/lib/libpord.a\
   $MUMPSDIR/lib/libmpiseq.a\
   $OPENBLASDIR/lib/libopenblas.a\
   -L$(pwd)/metis/lib -lmetis -lgomp -lpthread -lgfortran\
   -o benchmark_mumps_serial_openblas_openmp


export MUMPSDIR=$(pwd)/mumps_openmp_openblas_serial
export OPENBLASDIR=$(pwd)/openblas-serial
g++ -I $MUMPSDIR/include -O2 benchmark.cpp\
   $MUMPSDIR/lib/libdmumps.a\
   $MUMPSDIR/lib/libmumps_common.a\
   $MUMPSDIR/lib/libpord.a\
   $MUMPSDIR/lib/libmpiseq.a\
   $OPENBLASDIR/lib/libopenblas.a\
   -L$(pwd)/metis/lib -lmetis -lgomp -lpthread -lgfortran\
   -o benchmark_mumps_openmp_openblas_serial
